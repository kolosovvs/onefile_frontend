import { shallowMount } from '@vue/test-utils';
import Home from '@/components/Home.vue';


describe('Home.vue', () => {
  const wrapper = shallowMount(Home, { });

  it('check exists button select file', () => {
    expect(wrapper.contains('.select-file-button')).toBe(true);
  });

  it('check exists input for clipboard', () => {
    expect(wrapper.contains('.for-clipboard')).toBe(true);
  });

});
