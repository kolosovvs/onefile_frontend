import { shallowMount } from '@vue/test-utils';
import Contact from '@/components/Contact.vue';


describe('Contact.vue', () => {
  const wrapper = shallowMount(Contact, { });

  it('check exists email', () => {
    const email = wrapper.find('h2').text();
    expect(email).toBe('dev@onefile.space');
  });

});
