import { shallowMount } from '@vue/test-utils';
import Help from '@/components/Help.vue';


describe('Help.vue', () => {
  const wrapper = shallowMount(Help, { });

  it('check size', () => {
    const size = wrapper.find('.text-white').text();
    expect(size).toBe('Up to 10GB');
  });

});
