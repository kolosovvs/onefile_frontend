import { siteUrl } from './config';


export default function gitFileLink(data) {
  if (data.short_key) {
    return `${siteUrl}/${data.short_key}`;
  }
  return `${siteUrl}/${data.storage}/${data.key}/${data.file_name}`;
}
