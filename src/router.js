import Vue from 'vue';
import Router from 'vue-router';
import Home from './components/Home.vue';
import Help from './components/Help.vue';
import Contact from './components/Contact.vue';
import Short from './components/Short.vue';
import Rules from './components/Rules.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/help',
      name: 'help',
      component: Help,
    },
    {
      path: '/rules',
      name: 'rules',
      component: Rules,
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact,
    },
    {
      path: '/:short_key',
      name: 'short',
      component: Short,
    },
  ],
});
