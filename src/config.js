// export let apiUrl = 'https://onefile.space/api';
export const apiUrl = process.env.VUE_APP_API_URL || 'http://127.0.0.1:5000/api';
// export let siteUrl = 'https://onefile.space';
export const siteUrl = process.env.VUE_APP_SITE_URL || 'http://localhost:8081';
// export const wsChatUrl = 'wss://onefile.space/ws_chat';
export const wsChatUrl = process.env.VUE_APP_WS_CHAT_URL || 'ws://0.0.0.0:8010/ws_chat';

export const imageExtensions = ['jpg', 'jpeg', 'png', 'gif', 'svg', 'bmp'];
